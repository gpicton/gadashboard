﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="googleAnalyticsToDashboard._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style/reset.css" rel="stylesheet" type="text/css" />
    <link href="style/master.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/app.js" type="text/javascript"></script>
    <script src="Scripts/jquery.1.10.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="BodyContainer">
            <div id="InnerContainer">
                <div id="HeaderContainer">
                    <asp:Literal ID="litInfo" runat="server"></asp:Literal>
                </div>
                <div id="ContentContainer">
                    <table>
                        <tr>
                            <td>
                                <asp:FileUpload ID="fuFileContents" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnParseContents" runat="server" OnClick="btnParseContents_click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="FooterContainer">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
