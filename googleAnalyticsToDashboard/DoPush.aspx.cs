﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using googleAnalyticsToDashboard.classes;

namespace googleAnalyticsToDashboard
{
    public partial class DoPush : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!FileParsed)
            {
                StatusMessage = "File upload/parse failed";
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                rptOrdersInList.DataSource = Orders;
                rptOrdersInList.DataBind();
            }
        }
    }
}