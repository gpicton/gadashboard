﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoPush.aspx.cs" Inherits="googleAnalyticsToDashboard.DoPush" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="BodyContainer">
            <div id="InnerContainer">
                <div id="HeaderContainer">
                    <asp:Literal ID="litPush" runat="server"></asp:Literal>
                </div>
                <div id="ContentContainer">
                    <asp:Repeater ID="rptOrdersInList" runat="server">
                        <HeaderTemplate>
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            OrderID
                                        </th>
                                        <th>
                                            OrderTotal
                                        </th>
                                        <th>
                                            ShippingAmount
                                        </th>
                                        <th>
                                            OrderTax
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Sku
                                        </th>
                                        <th>
                                            Category
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                        <th>
                                            Quantity
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("OrderID") %></td>
                                <td><%# Eval("OrderTotal") %></td>
                                <td><%# Eval("ShippingAmount") %></td>
                                <td><%# Eval("OrderTax") %></td>
                                <td><%# Eval("Name") %></td>
                                <td><%# Eval("Sku") %></td>
                                <td><%# Eval("Category") %></td>
                                <td><%# Eval("Price") %></td>
                                <td><%# Eval("Quantity") %></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div id="FooterContainer">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
