﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using googleAnalyticsToDashboard.classes;

using System.Data.OleDb;
using System.Data;
using System.Text;
using System.IO;
using Excel;
using ICSharpCode;

namespace googleAnalyticsToDashboard
{
    public partial class _Default : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadPage();
            }
        }


        protected void btnParseContents_click(object sender, EventArgs e)
        {
            if (fuFileContents.HasFile)
            {

                string ext = Path.GetExtension(fuFileContents.FileName);

                if (ext == ".xls")
                {
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fuFileContents.FileContent);
                    excelReader.IsFirstRowAsColumnNames = true;
                    DataSet result = excelReader.AsDataSet();

                    FileParsed = ParseFile(result);
                }
                else if (ext == ".xlsx")
                {
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fuFileContents.FileContent);
                    excelReader.IsFirstRowAsColumnNames = true;
                    DataSet result = excelReader.AsDataSet();
                    FileParsed = ParseFile(result);
                }

                

                Response.Redirect("~/DoPush.aspx");
            }
            else
                litInfo.Text = "Please add file";
        }


        protected bool ParseFile(DataSet ds)
        {
            DataTable dt = ds.Tables[0];
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string OrderID = dr.ItemArray[0].ToString(); //dr.Field<string>("OrderID").ToString();
                    string OrderTotal = dr.ItemArray[1].ToString(); //dr.Field<string>("OrderTotal").ToString();
                    string ShippingAmount = dr.ItemArray[2].ToString(); //dr.Field<string>("ShippingAmount").ToString();
                    string OrderTax = dr.ItemArray[3].ToString(); //dr.Field<string>("OrderTax").ToString();
                    string Name = dr.ItemArray[4].ToString(); //dr.Field<string>("Name").ToString();
                    string Sku = dr.ItemArray[5].ToString(); //dr.Field<string>("Sku").ToString();
                    string Category = dr.ItemArray[6].ToString(); //dr.Field<string>("Category").ToString();
                    string Price = dr.ItemArray[7].ToString(); //dr.Field<string>("Price").ToString();
                    string Quantity = dr.ItemArray[8].ToString(); //dr.Field<string>("Quantity").ToString();

                    Order o = new Order(OrderID, OrderTotal, ShippingAmount, OrderTax, Name, Sku, Category, Price, Quantity);
                    Orders.Add(o);
                }
                return true;
            }
            catch (Exception e)
            {
                StatusMessage = e.Message;
                return false;
            }
        }

        protected void LoadPage()
        {
            FileParsed = false;
            Orders = new List<Order>();
        }
    }
}