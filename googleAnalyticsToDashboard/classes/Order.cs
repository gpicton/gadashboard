﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace googleAnalyticsToDashboard.classes
{
    public class Order
    {
        public string OrderID { get; set; }
        public string OrderTotal { get; set; }
        public string ShippingAmount { get; set; }
        public string OrderTax { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string Category { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }

        public Order() { }

        public Order (string OrderID, string OrderTotal, string ShippingAmount, string OrderTax, string Name, string Sku, string Category, string Price, string Quantity)
        {
            this.OrderID = OrderID;
            this.OrderTotal = OrderTotal;
            this.ShippingAmount = ShippingAmount;
            this.OrderTax = OrderTax;
            this.Name = Name;
            this.Sku = Sku;
            this.Category = Category;
            this.Price = Price;
            this.Quantity = Quantity;
        }
    }
}